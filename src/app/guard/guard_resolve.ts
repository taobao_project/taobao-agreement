import { WebQN_UserInfo } from "@/app/view/web-questionnaire/web-questionnaire.component";
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment"
@Injectable()
export class Guard_resolve implements Resolve<WebQN_UserInfo> {
  constructor(private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): WebQN_UserInfo | Observable<WebQN_UserInfo> | Promise<WebQN_UserInfo> | any {
    // TODO 开发环境下直接跳过
    if (environment.production === false) {
      return new WebQN_UserInfo('高翠兰', '16833@qq.com', '20', '女')
    }

    let consent = window.confirm('接下来需要您填写姓名、年龄、性别、微信信息，如果您同意就点击确认')
    if (consent === false) {
      this.router.navigate(['/'])
      return
    }
    let username = window.prompt('您的姓名')
    if (!username || !username.trim()) {
      this.router.navigate(['/'])
      return
    }
    let weChat = window.prompt('您的微信')
    if (!weChat || !weChat.trim()) {
      this.router.navigate(['/'])
      return
    }
    let age = window.prompt('您的年龄')
    if (!age || !age.trim()) {
      this.router.navigate(['/'])
      return
    }
    let gender = window.prompt('您的性别')
    if (!gender || !gender.trim()) {
      this.router.navigate(['/'])
      return
    }
    return new WebQN_UserInfo(username, weChat, age, gender)
  }

}