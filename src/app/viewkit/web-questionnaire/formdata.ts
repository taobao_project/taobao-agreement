export const web_hobbys = [
  { id: '1', name: '刷抖音' },
  { id: '2', name: '打游戏' },
  { id: '3', name: '关注某一领域的动态' },
  { id: '4', name: '写作/小说/文字/公众号' },
  { id: '5', name: '运动/户外/旅游' },
  { id: '6', name: '唱歌/跳舞/娱乐' },
  { id: '7', name: '发呆/不知道有什么爱好' },
]

export const web_genders = ['男', '女']

export const web_existUsernames = ['毛泽东', '邓小平', '习近平'];

export const web_motives = [
  { id: "1", name: '兴趣爱好' },
  { id: "2", name: '高薪目标' },
  { id: "3", name: '父母安排' },
  { id: "4", name: '朋友推荐' },
  { id: "5", name: '尝试新鲜' },
]

export const web_characters = [
  { id: "1", name: "稳重内敛" },
  { id: "2", name: "外向活泼" },
  { id: "3", name: "不善表达" },
  { id: "4", name: "多愁善感" },
  { id: "5", name: "任劳任怨" },
]

// 信息队列
export const web_msgs: Array<Object> = [];