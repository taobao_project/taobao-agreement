import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

// 判断选择爱好最大数
export function web_selectHobby(num: any) {
  return (val: any) => {
    return !val || val.length <= num;
  };
}

// 判断是否重复名字
export function web_checkName(value: any) {
  let res = true;
  if (this.existUsernames.indexOf(value) !== -1) {
    res = false;
  }
  return of(res).pipe(delay(500));
}

// 显示错误信息
export function web_showToast(type: any, title: string, msg: string) {
  this.msgs = [{ severity: type, summary: title, detail: msg }];
}