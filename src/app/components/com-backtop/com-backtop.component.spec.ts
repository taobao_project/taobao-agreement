import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComBacktopComponent } from './com-backtop.component';

describe('ComBacktopComponent', () => {
  let component: ComBacktopComponent;
  let fixture: ComponentFixture<ComBacktopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComBacktopComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComBacktopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
