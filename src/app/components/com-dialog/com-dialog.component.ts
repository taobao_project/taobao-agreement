import { Component, Input, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import SignaturePad from "signature_pad"
interface Inner {
  name: string,
  id: string,
  content: string,
  checked?: boolean,
  signature?: string
}
@Component({
  selector: 'app-com-dialog',
  templateUrl: './com-dialog.component.html',
  styleUrls: ['./com-dialog.component.scss']
})
export class ComDialogComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private doc: any) { }

  // 根据弹性变化做重绘
  resizeCanvas(signaturePad: any, canvas: any) {
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    signaturePad.clear(); // otherwise isEmpty() might return incorrect value
  }

  ngOnInit(): void {
    const canvas = this.doc.querySelector('#canvas')
    const signaturePad = new SignaturePad(canvas, {
      minWidth: 2,
      maxWidth: 2,
      penColor: "black"
    })
    signaturePad.addEventListener("endStroke", () => {
      this.data.signature = signaturePad.toDataURL()
    });
    window.addEventListener("resize", this.resizeCanvas.bind(this, signaturePad, canvas));
    this.resizeCanvas(signaturePad, canvas);
  }

  @Input()
  data!: Inner;
}
