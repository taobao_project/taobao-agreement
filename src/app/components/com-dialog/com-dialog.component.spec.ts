import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComDialogComponent } from './com-dialog.component';

describe('ComDialogComponent', () => {
  let component: ComDialogComponent;
  let fixture: ComponentFixture<ComDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
