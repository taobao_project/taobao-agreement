import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComBarComponent } from './com-bar.component';

describe('ComBarComponent', () => {
  let component: ComBarComponent;
  let fixture: ComponentFixture<ComBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComBarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
