import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgreementComponent } from './view/agreement/agreement.component';
import { WebQuestionnaireComponent } from './view/web-questionnaire/web-questionnaire.component';
import { AngularAppComponent } from './view/angular-app/angular-app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DevUIModule } from 'ng-devui';
import { ComBacktopComponent } from './components/com-backtop/com-backtop.component';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LibComponent } from './view/lib/lib.component';
import { ComBarComponent } from './components/com-bar/com-bar.component';
import { ComDialogComponent } from './components/com-dialog/com-dialog.component';
import { Guard_resolve } from './guard/guard_resolve';

@NgModule({
  declarations: [
    AppComponent,
    AgreementComponent,
    WebQuestionnaireComponent,
    AngularAppComponent,
    ComBacktopComponent,
    LibComponent,
    ComBarComponent,
    ComDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DevUIModule
  ],
  providers: [Location, {
    provide: LocationStrategy, useClass: HashLocationStrategy
  }, Guard_resolve],
  bootstrap: [AppComponent]
})
export class AppModule { }
