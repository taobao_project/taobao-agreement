import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Guard_resolve } from './guard/guard_resolve';
import { AgreementComponent } from './view/agreement/agreement.component';
import { AngularAppComponent } from './view/angular-app/angular-app.component';
import { LibComponent } from './view/lib/lib.component';
import { WebQuestionnaireComponent } from './view/web-questionnaire/web-questionnaire.component';

const routes: Routes = [
  { path: "", redirectTo: "app", pathMatch: "full" },
  { path: "app", component: AngularAppComponent },
  { path: "agr", component: AgreementComponent },
  { path: "lib", component: LibComponent },
  {
    path: "test", component: WebQuestionnaireComponent, resolve: {
      getData: Guard_resolve
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
