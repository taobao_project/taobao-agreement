import { DOCUMENT } from '@angular/common';
import data from "../../../assets/agreement"
import { Component, ElementRef, Inject } from '@angular/core';
import { DialogService } from 'ng-devui/modal';
import { ComDialogComponent } from 'src/app/components/com-dialog/com-dialog.component';
import { Router } from '@angular/router';
import { downloadPdf } from "@/utils/loadPdf"

interface Inner {
  name: string,
  id: string,
  content: string,
  checked?: boolean,
  signature?: string
}

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.scss']
})
export class AgreementComponent {
  // 数据源
  agrs: Inner[] = data.map((item: Inner) => {
    item.checked = false;
    return item
  })

  constructor(@Inject(DOCUMENT) private doc: any, private dialogService: DialogService, private router: Router) {
    let self = this;
    if (!window.navigator.userAgent.toLowerCase().match(/window/)) {
      // 如果不是window
      const results = this.dialogService.open({
        id: 'error',
        width: '346px',
        maxHeight: '600px',
        zIndex: 1050,
        backdropCloseable: true,
        html: true,
        dialogtype: 'failed',
        content: `<h1>当前页面只支持在PC端浏览！</h1>`,
        onClose() {
          self.router.navigate(['/']);
        },
        buttons: [
          {
            cssClass: 'primary',
            text: '明白！',
            handler: ($event: Event) => {
              results.modalInstance.hide();
            },
          },
        ],
      });
      setTimeout(() => {
        results.modalInstance.hide();
      }, 1500)
    }
  }

  // 打开弹窗
  openDialog(origin: Inner, pre: any): boolean | void {
    if (!origin.checked) {
      return false;
    }
    let timer!: any;
    let is_confirm = false;
    const results = this.dialogService.open({
      id: 'dialog-service',
      width: '60%',
      maxHeight: '80vh',
      title: origin.name + '  下方签字',
      content: ComDialogComponent,
      backdropCloseable: false,
      data: origin,
      dialogtype: 'standard',
      showAnimation: true,
      escapable: false,
      draggable: false,
      onClose(e: any): boolean | void {
        if (is_confirm) {
          return false;
        }
        origin.checked = false
      },
      buttons: [
        {
          id: 'btn-cancel',
          cssClass: 'common',
          text: '不同意',
          handler: ($event: Event) => {
            origin.checked = false
            results.modalInstance.hide();
          },
        },
        {
          cssClass: 'primary',
          text: '同意',
          disabled: false,
          handler: ($event: Event) => {
            is_confirm = true;
            pre.style.background = 'url(' + origin.signature + ')'
            downloadPdf(pre, origin.name)
            results.modalInstance.hide();
          },
        }
      ],
    });
    let time = 5;
    results.modalInstance.updateButtonOptions([null, { text: '倒计时。。。', disabled: true }]);
    timer = setInterval(() => {
      if (time <= 0) {
        results.modalInstance.updateButtonOptions([null, { text: '我同意', disabled: false }]);
        clearInterval(timer)
      } else {
        results.modalInstance.updateButtonOptions([null, { text: `阅读计时：${time}` }]);
        time--
      }
    }, 1000)
  }

  // 接收子组件发过来的数据
  onconfirm(data: Inner) {
    console.log(data);
  }

}
