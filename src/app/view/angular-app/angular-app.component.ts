import { Component, OnInit } from '@angular/core';
import copyToClipboard from 'src/utils/copy';

@Component({
  selector: 'app-angular-app',
  templateUrl: './angular-app.component.html',
  styleUrls: ['./angular-app.component.scss']
})
export class AngularAppComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {

  }
  title = "前端在线"

  copy(event: any) {
    copyToClipboard(event)
  }
}
