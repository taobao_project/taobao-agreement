import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lib',
  templateUrl: './lib.component.html',
  styleUrls: ['./lib.component.scss']
})
export class LibComponent implements OnInit {
  library = [
    {
      name: "vue2精品工具",
      link: "https://awesome-vue.js.org/"
    },
    {
      name: "vue3精品工具",
      link: "https://next.awesome-vue.js.org/"
    },
    {
      name: "react精品工具",
      link: "https://github.com/enaqx/awesome-react"
    },
    {
      name: "react-query强大的请求管理",
      link: "https://cangsdarm.github.io/react-query-web-i18n/"
    },
    {
      name: "阿里图标库",
      link: "https://www.iconfont.cn/"
    },
    {
      name: "各行业数据",
      link: "https://github.com/awesomedata/awesome-public-datasets#machinelearning"
    },
    {
      name: "Python资源大集合",
      link: "https://github.com/vinta/awesome-python"
    },
    {
      name: "go资源精选大全",
      link: "https://github.com/avelino/awesome-go"
    },
    {
      name: "后端架构师技术图谱",
      link: "https://github.com/xingshaocheng/architect-awesome"
    },
    {
      name: "机器学习资源列表",
      link: "https://github.com/josephmisiti/awesome-machine-learning"
    },
    {
      name: "最全icon图标",
      link: "https://fontawesome.com/icons"
    },
    {
      name: "野马工作室",
      link: "https://yc93nl.yuque.com/yc93nl"
    },
    {
      name: "尚硅谷springBoot2",
      link: "https://www.yuque.com/atguigu/springboot"
    },
    {
      name: "前端笔记",
      link: "https://www.yuque.com/u28109111/xfahvq"
    },
    {
      name: "宋宇博客",
      link: "http://websong.wang/"
    },
    {
      name: "宋宇文档",
      link: "http://doc.websong.wang/"
    },
    {
      name: "宋宇面试",
      link: "http://interview.websong.wang/"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
