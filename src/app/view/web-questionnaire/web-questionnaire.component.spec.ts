import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebQuestionnaireComponent } from './web-questionnaire.component';

describe('WebQuestionnaireComponent', () => {
  let component: WebQuestionnaireComponent;
  let fixture: ComponentFixture<WebQuestionnaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebQuestionnaireComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WebQuestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
