/// <reference path='copy.d.ts' />

export default function copyToClipboard(div: Element) {
  var range
  var body: any = document.body
  if (body.createTextRange) {
    range = body.createTextRange();
    range.moveToElementText(div);
    range.select();
  } else if (window.getSelection) {
    var selection: any = window.getSelection();
    range = document.createRange();
    range.selectNodeContents(div); // 创建选取内容范围
    selection.removeAllRanges();  // 清除已选择的内容
    selection.addRange(range);   // 添加选取内容，模拟用户选取
    /*if(selection.setBaseAndExtent){
        selection.setBaseAndExtent(text, 0, text, 1);
    }*/
  } else {
    console.warn("none");
  }
  document.execCommand('copy');   // 触发复制事件
  // alert("复制成功")
  document.execCommand("unselect", true)
}