
import html2canvas from "html2canvas"
import { jsPDF as JsPDF } from "jspdf";
export const downloadPdf = (dom: any, name: string = '前端在线',
  w_times?: number, h_times?: number
) => {
  html2canvas(dom).then((canvas: any) => {
    const contentWidth = canvas.width;
    const contentHeight = canvas.height;
    // 一页pdf显示html页面生成的canvas高度;
    const pageHeight = contentWidth / 592.28 * 841.89;
    // 未生成pdf的html页面高度
    let leftHeight = contentHeight;
    // 页面偏移
    let position = 0;
    // a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
    const imgWidth = 595.28;
    const imgHeight = 592.28 / contentWidth * contentHeight;

    const pageData = canvas.toDataURL('image/jpeg', 2);

    const pdf = new JsPDF('l', 'pt', 'a4'); // 纵向方向p，单位pt|mm， 格式a4

    // 有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
    // 当内容未超过pdf一页显示的范围，无需分页
    if (leftHeight < pageHeight) {
      pdf.addImage(pageData, 'JPEG', 10, 10, imgWidth * (w_times || 1.5), imgHeight * (h_times || 1.5));
    } else {
      while (leftHeight > 0) {
        pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
        leftHeight -= pageHeight;
        position -= 841.89;
        // 避免添加空白页
        if (leftHeight > 0) {
          pdf.addPage();
        }
      }
    }
    pdf.save(name);
  })
}